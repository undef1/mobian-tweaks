mobian-tweaks (0.3.5) unstable; urgency=medium

  * tweaks: remove files already in Debian's mobile-tweaks-common
  * d/control: depend on mobile-tweaks-common

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Wed, 04 Nov 2020 15:46:16 +0100

mobian-tweaks (0.3.4) unstable; urgency=medium

  [ Arnaud Ferraris ]
  * prerm: fix pam profile name
  * d/control: remove relationships with deprecated pinephone-devtools
  * dconf: enable scale-to-fit for evolution-alarm-notify
  * gschema: remove deprecated Epiphany key 'mobile-user-agent'

  [ Koen Vervloesem ]
  * Generate SSH host keys on first boot

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 27 Oct 2020 15:42:46 +0100

mobian-tweaks (0.3.3) unstable; urgency=medium

  * tweaks: drop feedbackd theme as it is device-specific

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 03 Oct 2020 23:55:26 +0200

mobian-tweaks (0.3.2) unstable; urgency=medium

  * postinst: fix diversion removal

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 29 Sep 2020 12:55:37 +0200

mobian-tweaks (0.3.1) unstable; urgency=medium

  * 90_mobian.gschema: make sure on-screen keyboard is enabled.
  * tweaks: remove firefox-esr default config
  * dconf: stop scaling firefox.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 29 Sep 2020 09:49:00 +0200

mobian-tweaks (0.3.0) unstable; urgency=medium

  * applications: remove no longer necessary firefox-esr.desktop.
    This was a workaround for FF 68, which is not longer needed as a proper
    fix is now included in FF 78.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sun, 27 Sep 2020 17:41:33 +0200

mobian-tweaks (0.2.9) unstable; urgency=medium

  * tweaks: make sure USB network is setup properly.
    When creating an image, USB networking setup fails because we're in a
    chroot and NetworkManager isn't running.
    If that happens, we need to setup a systemd service (running only on
    first boot) to create the NM connection.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 22 Sep 2020 00:33:31 +0200

mobian-tweaks (0.2.8) unstable; urgency=medium

  * 90_mobian.gschema: remove unnecessary settings.
    The `button-layout` default setting is now provided by
    `phosh-mobile-tweaks`, it can therefore be removed
  * phoc-scaling: stop scaling gnome-maps.
    This app is now fully mobile-friendly
  * 90_mobian.gschema: keep showing the date in phosh top bar

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 19 Sep 2020 14:12:34 +0200

mobian-tweaks (0.2.7) unstable; urgency=medium

  * d/postinst: use a less common network address.
    Fixes https://gitlab.com/mobian1/issues/-/issues/88
  * mobian-usb-gadget.sh: make more robust and add logs
  * mobian-usb-gadget.service: make it possible to restart the service
  * debian: get rid of mobian-mtp.service.
  * tweaks: improve SD card automounting.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Fri, 04 Sep 2020 17:52:48 +0200

mobian-tweaks (0.2.6) unstable; urgency=medium

  [ Sebastian Spaeth ]
  * mobian-tweaks: overide pam.d/common-password to allow numeric passwords
    Fixes https://gitlab.com/mobian1/issues/-/issues/9

  [ Arnaud Ferraris ]
  * tweaks: add script for configuring USB gadget
  * tweaks: add custom config file for umtp-responder

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 03 Sep 2020 23:37:54 +0200

mobian-tweaks (0.2.5) unstable; urgency=medium

  [ Arnaud Ferraris ]
  * applications: telegramdesktop: change global scale factor
  * 90_mobian.gschema: restore desktop animations

  [ Sebastian Spaeth ]
  * resize-rootfs: Inform user while the rootfs is resized

 -- Sebastian Spaeth <Sebastian@SSpaeth.de>  Thu, 27 Aug 2020 08:11:27 +0200

mobian-tweaks (0.2.4) unstable; urgency=medium

  * debian: remove resize-rootfs systemd service

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 11 Aug 2020 23:04:37 +0200

mobian-tweaks (0.2.3) unstable; urgency=medium

  * resize-rootfs: add F2FS compatibility and move to initramfs

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 04 Aug 2020 23:50:25 +0200

mobian-tweaks (0.2.2) unstable; urgency=medium

  * firefox-esr: set preferences using defaultPref
    pref() will reset to the default value every time firefox-esr is
    started.
    defaultPref() sets the default value once and allows the user to change
    it, keeping the modified value over time.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Fri, 24 Jul 2020 12:38:07 +0200

mobian-tweaks (0.2.1) unstable; urgency=medium

  * telegramdesktop: scale to 95% by default.
    This will fit the 360x720 resolution without the need to use phoc's
    scale-to-fit feature.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Tue, 14 Jul 2020 17:52:24 +0200

mobian-tweaks (0.2.0) unstable; urgency=medium

  * mobian.json: update feedbackd profile
  * 90_mobian.gschema: fix auto-suspend
  * tweaks: add dconf defaults for auto-scaling widely used apps

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sun, 12 Jul 2020 16:19:02 +0200

mobian-tweaks (0.1.9) unstable; urgency=medium

  * tweaks: resize rootfs on first boot

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 27 Jun 2020 13:13:04 +0200

mobian-tweaks (0.1.8) unstable; urgency=medium

  * 90_mobian.gschema: disable auto-brightness and enable auto-suspend

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 27 Jun 2020 11:16:18 +0200

mobian-tweaks (0.1.7) unstable; urgency=medium

  [ Tomasz Oponowicz ]
  * Improve Firefox UI and performance

  [ Arnaud Ferraris ]
  * firefox-esr: keep standard homepage and revert to "compact" UI density

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Fri, 26 Jun 2020 17:03:00 +0200

mobian-tweaks (0.1.6) unstable; urgency=medium

  * 99-automount-sd: only mount when accessed

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Fri, 26 Jun 2020 16:44:49 +0200

mobian-tweaks (0.1.5) unstable; urgency=medium

  [ Tomasz Oponowicz ]
  * Disable Gnome animations

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 13 Jun 2020 15:54:27 +0200

mobian-tweaks (0.1.4) unstable; urgency=medium

  * tweaks: lighten vibration on button-pressed and window-closed events

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Mon, 25 May 2020 11:05:57 +0200

mobian-tweaks (0.1.3) unstable; urgency=medium

  * firefox-esr: fix youtube user-agent override

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Mon, 25 May 2020 01:36:39 +0200

mobian-tweaks (0.1.2) unstable; urgency=medium

  * tweaks: change feedback theme for new feedbackd version
  * firefox-esr: use more standard user-agent
  * firefox-esr: add youtube-specific user-agent override

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Mon, 25 May 2020 01:23:40 +0200

mobian-tweaks (0.1.1) unstable; urgency=medium

  * tweaks: automount SD when inserted

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Mon, 11 May 2020 12:07:06 +0200

mobian-tweaks (0.1.0) unstable; urgency=medium

  * Initial release based on `pinephone-tweaks` v0.14

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 07 May 2020 12:30:50 +0200
